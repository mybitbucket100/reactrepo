import React from 'react'
import TestComponent from './TestComponent'

// const App = () => 
// {
// 	return(
// 		<div>
// 			<h1>Hello World</h1>
// 			<TestComponent name='Satya'/>
// 		</div>
// 	);
// };

class App extends React.Component
{
	// constructor(props)
	// {
	// 	super(props)
	// 	this.state={name:null};
		
	// }
	state={name:null}

	componentDidMount()
	{
		this.setState({name:'Satya'})
	}
	render()
	{
		
		console.log(this.state.name)
		return(
			<div>
				<h1>Hello World</h1>
				<TestComponent name={this.state.name||'UNK'}/>
			</div>
		)
	}
}
export default App;